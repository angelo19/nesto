
export =  {
  type: "mssql",
  host: process.env.DB_HOST || "localhost",
  port: 1433,
  username: 'SA',
  password: "P@ssw0rd",
  database: 'LocalDev',
  entities: [
    'src/**/**.entity{.ts,.js}',
  ],
  migrations: [
    'src/database/migrations/*.ts',
  ],
  cli: {
    migrationsDir: 'src/database/migrations',
  },
  synchronize: true
}