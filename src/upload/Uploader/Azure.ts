import { UploaderInterface } from "./types";

export interface AzureConfigs {
  AZURE_STORAGE_ACCOUNT: string
  AZURE_STORAGE_ACCESS_KEY: string
  AZURE_STORAGE_CONNECTION_STRING
}

export default class AzureUploader implements UploaderInterface {
  private configs = {}

  // any return type for now
  upload(file, params): any {
    return 'something'
  }
}
