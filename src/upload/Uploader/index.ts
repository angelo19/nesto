import Azure from './Azure'
import { UploaderInterface } from './types'

export default class Uploader {
  static create(provider = Azure): UploaderInterface {

    // azure for now
    return new Azure()
  }
}