export enum PROVIDERS {
  AZURE,
  GOOGLE
}

export interface UploaderInterface {
  upload (file: any, params: any): any
}