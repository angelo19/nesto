import { Injectable } from '@nestjs/common';

import { UploaderInterface } from './Uploader/types'
import Uploader from './Uploader'

@Injectable()
export class UploadService {
  uploader: UploaderInterface

  constructor() {
    const uploader = Uploader.create()

    this.uploader = uploader
  }
}
