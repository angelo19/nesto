import {
  Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';
import { ConstantsService } from '@app/constants'
import { CreateTodoDto } from './dto/create-todo';
import { ApiResponse } from '@nestjs/swagger';
import { TodoService } from './todo.service';
import { Todo } from './todo.entity';

@Controller('v1/todo')
export class TodoController {

  // this is the dependency injection
  constructor(
    private readonly todoService: TodoService,
    private readonly constantsService: ConstantsService
    ) {}

  @Get()
  @ApiResponse({
    status: 200,
    description: 'Found',
    type: [Todo]
  })
  findAll() {
    return this.todoService.findAll();
  }

  @Post()
  @ApiResponse({
    status: 201,
    description: 'Created',
    type: Todo
  })
  create(@Body() createTodoDto: CreateTodoDto) {
    return this.todoService.create(createTodoDto);
  }

  @Put(':id')
  update(): string {
    return 'hello';
  }

  @Get(':id')
  @ApiResponse({
    status: 200,
    description: 'Found',
    type: Todo
  })
  @ApiResponse({
    status: 404,
    description: 'If todo not found',
    // type: Todo // todo type error
  })
  findOne(@Param('id') id: number) {
    return this.todoService.findOne(id);
  }

  @Delete(':id')
  delete(): string {
    return 'hello';
  }

  @Get('/showCaseLib')
  showCaseLib(): object {

    // this a sample where other modules are injected to this module
    return this.constantsService.clientLinks()
  }
}
