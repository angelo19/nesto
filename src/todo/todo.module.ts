import { Module } from '@nestjs/common';
import { TodoService } from './todo.service';
import { TodoController } from './todo.controller';
import { ConstantsModule } from '@app/constants';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Todo } from './todo.entity';

@Module({
  providers: [TodoService],
  imports: [TypeOrmModule.forFeature([Todo]),ConstantsModule],
  controllers: [TodoController]
})
export class TodoModule {}
