import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateTodoDto } from './dto/create-todo';
import { Todo } from './todo.entity';

@Injectable()
export class TodoService {

  // this is the dependency injection
  constructor(
    @InjectRepository(Todo)
    private todoRepository: Repository<Todo>,
  ) {}

  async create(createTodoDto: CreateTodoDto): Promise<Todo> {
    this.todoRepository.create(createTodoDto)
    const todo = await this.todoRepository.save(createTodoDto)
    return todo
  }

  async findAll(): Promise<Todo[]> {
    const todos = await this.todoRepository.find()
    return todos
  }

  async findOne(id: number): Promise<Todo> {
    const todo = await this.todoRepository.findOne(id)

    if(!todo) {
      throw new NotFoundException()
    }
    return todo
  }
}
