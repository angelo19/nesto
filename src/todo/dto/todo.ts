export class TodoDto {
  title: string;
  body: string;
  createdAt: string
  updatedAt: string
}