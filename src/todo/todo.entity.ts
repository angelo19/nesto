import { ApiProperty } from '@nestjs/swagger';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('todo')
export class Todo {
  @ApiProperty()
  @PrimaryGeneratedColumn()
  id: number;

  @ApiProperty()
  @Column({ default: true, name: 'IsActive' })
  isActive: boolean;

  @ApiProperty()
  @Column('varchar', { name: 'Title', default: '' })
  title: string;

  @ApiProperty()
  @Column('varchar', { name: 'Body' })
  body: string;
}