import "reflect-metadata";

import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TodoModule } from './todo/todo.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { StatusModule } from '@app/status'
import { UploadModule } from './upload/upload.module';

@Module({
  imports: [
    TodoModule,
    StatusModule,
    TypeOrmModule.forRoot({
      type: "mssql",
      host:  "localhost",
      port: 1433,
      username: 'SA',
      password: "P@ssw0rd",
      database: 'LocalDev',
      entities: [],
      autoLoadEntities: true,
      // migrations: [
      //   'src/database/migrations/*.ts',
      // ],
      // cli: {
      //   migrationsDir: 'src/database/migrations',
      // },
      synchronize: true
    }),
    UploadModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
