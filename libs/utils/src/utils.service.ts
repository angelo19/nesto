import { Injectable } from '@nestjs/common';
import StringUtil from './lib/string'

@Injectable()
export class UtilsService {
  static get StringUtil() {
    return StringUtil
  }
}
