import { Controller, Get } from '@nestjs/common';

@Controller('v1/status')
export class StatusController {
  @Get()
  status(): object {

    // get env variables
    return {
      gitHash: 'xzczxczxc',
      env: 'development',
    };
  }
}
