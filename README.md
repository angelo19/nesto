<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo_text.svg" width="320" alt="Nest Logo" /></a>
</p>

## Description

Built with [Nest](https://github.com/nestjs/nest)

Concepts

[Dependency Injection](/)
[Controllers](https://docs.nestjs.com/controllers),
[Providers(services)](https://docs.nestjs.com/providers),
[Modules](https://docs.nestjs.com/modules),
[Dependency Injection in NestJs](https://docs.nestjs.com/fundamentals/custom-providers),
[Database](https://docs.nestjs.com/techniques/database),
[Columns types for MSSQL](https://typeorm.io/#/entities/column-types-for-mssql)
[Libs Folder](./libs/README.md)

## Prerequisites

1. Download Node v12.19.0. Recommended to use node version manager (https://github.com/nvm-sh/nvm)

2. yarn package manager https://yarnpkg.com/

3. VScode, Add extenstion `Prettier - Code formatter`, `Eslint`, `Docker`

In vscode settings.json

```
      "editor.codeActionsOnSave": {
        "source.fixAll.eslint": true
      },
```

4. Docker and docker-compose https://docs.docker.com/get-docker/

5. Azure Data studio for the GUI of the database https://docs.microsoft.com/en-us/sql/azure-data-studio/download-azure-data-studio?view=sql-server-ver15

6. `sqlcmd` cli https://docs.microsoft.com/en-us/sql/linux/sql-server-linux-setup-tools?view=sql-server-ver15

## Dev Setup and Installation

```bash
$ yarn install

# development
$ docker-compose -f docker-compose-dev.yml up -d # to run the mssql server on a docker container
$ cp .env.example .env

$ sqlcmd -S localhost -U SA -P "P@ssw0rd" -Q "CREATE DATABASE LocalDev" # Creating Local Database Run only once
$ yarn start:dev

# open the api on http://localhost:3000

```

## Documentation

- API Documentation in swagger http://localhost:3000/swagger
- Modules documentation run `$ yarn doc` and open http://localhost:8080

## Test

```bash
# unit tests
$ yarn test

# e2e tests
$ yarn test:e2e

# test coverage
$ yarn test:cov
```

## Scripts

- Sync database Entity/Table (BACKUP DATABASE IF PROD DB)

```bash
$ yarn run typeorm:cli schema:sync
```
